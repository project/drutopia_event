<!-- writeme -->
Drutopia Event
==============

Drutopia Event is a base feature providing a event content type, with date and event type, and related configuration.

 * https://gitlab.com/drutopia/drutopia_event
 * Issues: https://gitlab.com/drutopia/drutopia_event/issues
 * Source code: https://gitlab.com/drutopia/drutopia_event/tree/8.x-1.x
 * Keywords: events, date, calendar, drutopia
 * Package name: drupal/drutopia_event


### Requirements

 * drupal/block_visibility_groups ^2
 * drupal/config_actions ^1.1
 * drupal/ctools ^3.4
 * drupal/drutopia_seo ^1.0
 * drupal/ds ^3.7
 * drupal/facets ^2
 * drupal/pathauto ^1.8
 * drupal/token ^1.7


### License

GPL-2.0+

<!-- endwriteme -->
